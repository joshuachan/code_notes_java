class Data {
    private int[] arr = null;
    private int len = 0;
    private int times = 0;

    public Data(int[] arr) {
        this.arr = arr;
        len = arr.length;
        times = 5;
    }

    private void relax() {
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
        }
    }

    private class GetData extends Thread {
        public void run() {
            for (int i = 0; i < times; i++) {
                for (int j = 0; j < len; j++) {
                    while (true) {
                        synchronized (this) {
                            if (arr[j] != 0) {
                                System.out.printf("Get arr[%d] = %d\n", j, arr[j]);
                                arr[j] = 0;
                                break;
                            }
                        }
                        relax();
                    }
                }
            }
        }
    }

            
    private class PutData extends Thread {
        public void run() {
            for (int i = 0; i < times; i++) {
                for (int j = 0; j < len; j++) {
                    synchronized (this) {
                        arr[j] = (int)(Math.random() * 100) + 1;
                        System.out.printf("Put arr[%d] = %d\n", j, arr[j]);
                    }
                    relax();
                }
            }
        }
    }

    public void start() {
        GetData get = new GetData();
        PutData put = new PutData();

        get.start();
        put.start();
    }
}

public class Test2a {
    public static void main(String[] args) {
        int[] arr = new int[10];
        Data data = new Data(arr);

        data.start();
    }
}