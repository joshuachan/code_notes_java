class Locker {
    private int count = 0;
    private int index = 0;

    public Locker(int count) {
        this.count = count;
        index = count;
    }

    public void enter() {
        while (true) {
            synchronized (this) {
                if (index > 0) {
                    index--;
                    break;
                } else {
                    try {
                        wait();
                    } catch (InterruptedException e) {
                    }
                }
            }
        }
    }

    public void exit() {
        synchronized (this) {
            if (index < count)
                index++;
            notifyAll();
        }
    }
}

class Data {
    private int[] arr = null;
    private int len = 0;
    private int times = 0;
    Locker locker = null;

    public Data(int[] arr) {
        this.arr = arr;
        len = arr.length;
        locker = new Locker(1);
        times = 5;
    }

    private void relax() {
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
        }
    }

    private class GetData extends Thread {
        public void run() {
            for (int i = 0; i < times; i++) {
                for (int j = 0; j < len; j++) {
                    while (true) {
                        locker.enter();
                        if (arr[j] != 0) {
                            System.out.printf("Get arr[%d] = %d\n", j, arr[j]);
                            arr[j] = 0;
                            locker.exit();
                            break;
                        } else {
                            locker.exit();
                            relax();
                        }
                    }
                }
            }
        }
    }
            
    private class PutData extends Thread {
        public void run() {
            for (int i = 0; i < times; i++) {
                for (int j = 0; j < len; j++) {
                    locker.enter();
                    arr[j] = (int)(Math.random() * 100) + 1;
                    System.out.printf("Put arr[%d] = %d\n", j, arr[j]);
                    locker.exit();
                    relax();
                }
            }
        }
    }

    public void start() {
        GetData get = new GetData();
        PutData put = new PutData();

        put.start();
        get.start();
    }
}

public class Test2 {
    public static void main(String[] args) {
        int[] arr = new int[10];
        Data data = new Data(arr);

        data.start();
    }
}