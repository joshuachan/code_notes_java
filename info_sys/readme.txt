简易信息管理系统（Java语言描述）

　　通过2个双向环形链表实现教师和学生信息的管理。
　　主要实现操作如下：
　　1、教师/学生信息记录添加
　　2、按ID或Name查找或删除教师/学生信息
　　3、数据文件的自动导入和导出

　　程序共分8个类文件：
Test，UserInterface，LinkList，Person，Teacher，Student，TextFileReader，TextFileWriter。
其中，Person，Teacher，Student，TextFileReader，TextFileWriter由Delphi Tang提供。