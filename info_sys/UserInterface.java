/* ******************************************************
 * FileName: UserInterface.java
 * Project: 师生信息管理系统
 * Description: 通过双向环形链表实现教师和学生信息的分开管理
 *              主要操作有:
 *              1. 记录添加
 *              2. 按ID或Name查找或删除记录
 *              3. 数据的自动导入和导出
 * Author & Date: Joshua Chan, 2011/11/14
 * *****************************************************/
import java.util.Scanner;

/* 用户界面类, 实现用户界面交互及相关方法调用        */
public class UserInterface {
    private LinkList lt;
    private LinkList ls;
    private String title = "\n***** Infomation Management System *****";
    private String sep = "========================================";

    /* 初始化, 引入teacher链表类和student链表类        */
    public UserInterface(LinkList teacher, LinkList student) {
        lt = teacher;
        ls = student;
    }

    /* 界面顶部信息        */
    public void dispTitle() {
        System.out.println(title);
        System.out.println(sep);
    }

    /* 获取String        */
    public String getString() {
        Scanner input = new Scanner(System.in);
        String s = input.nextLine();
        return s.trim();
    }

    /* 获取int,    先获得String, 再转换为int    */
    public int getInt() {
        int n = -999;
        while (n == -999) {
            try {
                n = Integer.parseInt(getString());
            } catch(Exception e) {
                System.out.print("Invalid number, try again: ");
            }
        }
        return n;
    }

    /* 显示主界面        */
    public void dispMain() {
        dispTitle();
        System.out.print("  [1] Teacher Information Management\n"
                        +"  [2] Student Information Management\n\n"
                        +"  [0] Exit\n\n");
    }

    /* 控制主界面的选择操作        */
    public void selectMain() {
        System.out.print("Please input choice: ");
        int n = getInt();
        switch (n) {
        case 1:
            dispTeacher();
            selectTeacher();
            break;
        case 2:
            dispStudent();
            selectStudent();
            break;
        case 0:
            return;
        default:
            System.out.println("Invalid choice.");
            selectMain();
            break;
        }
    }

    /* 显示教师管理界面        */
    public void dispTeacher() {
        dispTitle();
        System.out.print("  [1] Add Teacher Information\n"
                        +"  [2] Teacher Information Query\n"
                        +"  [3] Search Teacher by ID\n"
                        +"  [4] Search Teacher by Name\n"
                        +"  [5] Remove Teacher by ID\n"
                        +"  [6] Remove Teacher by Name\n\n"
                        +"  [0] Return\n\n");
    }

    /* 控制教师管理界的选择操作        */
    public void selectTeacher() {
        System.out.print("Please input choice: ");
        int n = getInt();
        switch (n) {
        case 1:
            addTeacher();
            break;
        case 2:
            travelTeacher();
            break;
        case 3:
            searchTeacherByID();
            break;
        case 4:
            searchTeacherByName();
            break;
        case 5:
            removeTeacherByID();
            break;
        case 6:
            removeTeacherByName();
            break;
        case 0:
            dispMain();
            selectMain();
            break;
        default:
            System.out.println("Invalid choice.");
            selectTeacher();
            break;
        }
    }

    /* 显示学生管理界面        */
    public void dispStudent() {
        dispTitle();
        System.out.print("  [1] Add Student Information\n"
                        +"  [2] Student Information Query\n"
                        +"  [3] Search Student by ID\n"
                        +"  [4] Search Student by Name\n"
                        +"  [5] Remove Student by ID\n"
                        +"  [6] Remove Student by Name\n\n"
                        +"  [0] Return\n\n");
    }

    /* 控制学生管理界的选择操作        */
    public void selectStudent() {
        System.out.print("Please input choice: ");
        int n = getInt();
        switch (n) {
        case 1:
            addStudent();
            break;
        case 2:
            travelStudent();
            break;
        case 3:
            searchStudentByID();
            break;
        case 4:
            searchStudentByName();
            break;
        case 5:
            removeStudentByID();
            break;
        case 6:
            removeStudentByName();
            break;
        case 0:
            dispMain();
            selectMain();
            break;
        default:
            System.out.println("Invalid choice.");
            selectStudent();
            break;
        }
    }

    /* 教师信息添加
     * 字段间以英文逗号分隔, 6个字段才会被判为有效
     * 多余空格将被自动删除        */
    public void addTeacher() {
        Teacher t;
        String[] sa;
        String str;
        while (true) {
            System.out.println("\nPlease input infomation or just press enter to return.");
            System.out.println("Format: ID, name, gender, course, position, department");
            str = getString();
            if ((str.trim()).length() == 0) {
                dispTeacher();
                selectTeacher();
                break;
            }
            sa = str.split(",");
            if (sa.length == 6) {
                t = new Teacher(sa[0].trim(), sa[1].trim(), sa[2].trim(),
                                sa[3].trim(), sa[4].trim(), sa[5].trim());
                lt.addLast(t);
                System.out.print("New record was added, press enter to continue... ");
            } else {
                System.out.print("Wrong format, press enter to continue... ");
            }
            getString();
        }
    }

    /* 学生信息添加
     * 字段间以英文逗号分隔, 4个字段才会被判为有效
     * 多余空格将被自动删除        */
    public void addStudent() {
        Student s;
        String[] sa;
        String str;
        while (true) {
            System.out.println("\nPlease input infomation or just press enter to return.");
            System.out.println("Format: ID, name, gender, department");
            str = getString();
            if ((str.trim()).length() == 0) {
                dispStudent();
                selectStudent();
                break;
            }
            sa = str.split(",");
            if (sa.length == 4) {
                s = new Student(sa[0].trim(), sa[1].trim(), sa[2].trim(),
                                sa[3].trim());
                ls.addLast(s);
                System.out.print("New record was added, press enter to continue... ");
            } else {
                System.out.print("Wrong format, press enter to continue... ");
            }
            getString();
        }
    }

    /* 遍历打印教师信息        */
    public void travelTeacher() {
        if (!(lt.travelList()))
            System.out.print("\nThere is no record.\n");
        System.out.print("Press enter to continue... ");
        getString();
        dispTeacher();
        selectTeacher();
    }
        
    /* 遍历打印学生信息        */
    public void travelStudent() {
        if (!(ls.travelList()))
            System.out.print("\nThere is no record.\n");
        System.out.print("Press enter to continue... ");
        getString();
        dispStudent();
        selectStudent();
    }
    
    /* 根据ID查找教师信息, 并打印        */
    public void searchTeacherByID() {
        System.out.print("Please input ID number: ");
        String str = getString();
        if (lt.searchByID(str))
            System.out.print("Record was found, press enter to continue... ");
        else
            System.out.print("Record not found, press enter to continue... ");
        getString();
        dispTeacher();
        selectTeacher();
    }
        
    /* 根据ID查找学生信息, 并打印        */
    public void searchStudentByID() {
        System.out.print("Please input ID number: ");
        String str = getString();
        if (ls.searchByID(str))
            System.out.print("Record was found, press enter to continue... ");
        else
            System.out.print("Record not found, press enter to continue... ");
        getString();
        dispStudent();
        selectStudent();
    }

    /* 根据Name查找教师信息, 并打印        */
    public void searchTeacherByName() {
        System.out.print("Please input name: ");
        String str = getString();
        if (lt.searchByName(str))
            System.out.print("Record was found, press enter to continue... ");
        else
            System.out.print("Record not found, press enter to continue... ");
        getString();
        dispTeacher();
        selectTeacher();
    }

    /* 根据Name查找学生信息, 并打印        */
    public void searchStudentByName() {
        System.out.print("Please input name: ");
        String str = getString();
        if (ls.searchByName(str))
            System.out.print("Record was found, press enter to continue... ");
        else
            System.out.print("Record not found, press enter to continue... ");
        getString();
        dispStudent();
        selectStudent();
    }

    /* 根据ID查找教师信息, 并打印, 待用户确认后删除该记录        */
    public void removeTeacherByID() {
        System.out.print("Please input ID number: ");
        String str = getString();
        if (lt.searchByID(str)) {
            System.out.print("Record was found, REMOVE it please input y,\n"
                            +"or just press enter to cancel: ");
            if ((getString()).equalsIgnoreCase("y")) {
                lt.removeByID(str);
                System.out.print("Record was removed, press enter to continue... ");
                getString();
            }
        } else {
            System.out.print("Record not found, press enter to continue... ");
            getString();
        }
        dispTeacher();
        selectTeacher();
    }

    /* 根据ID查找学生信息, 并打印, 待用户确认后删除该记录        */
    public void removeStudentByID() {
        System.out.print("Please input ID number: ");
        String str = getString();
        if (ls.searchByID(str)) {
            System.out.print("Record was found, REMOVE it please input y,\n"
                            +"or just press enter to cancel: ");
            if ((getString()).equalsIgnoreCase("y")) {
                ls.removeByID(str);
                System.out.print("Record was removed, press enter to continue... ");
                getString();
            }
        } else {
            System.out.print("Record not found, press enter to continue... ");
            getString();
        }
        dispStudent();
        selectStudent();
    }

    /* 根据Name查找教师信息, 并打印, 待用户确认后删除该记录        */
    public void removeTeacherByName() {
        System.out.print("Please input name: ");
        String str = getString();
        if (lt.searchByName(str)) {
            System.out.print("Record was found, REMOVE it please input y,\n"
                            +"or just press enter to cancel: ");
            if ((getString()).equalsIgnoreCase("y")) {
                lt.removeByName(str);
                System.out.print("Record was removed, press enter to continue... ");
                getString();
            }
        } else {
            System.out.print("Record not found, press enter to continue... ");
            getString();
        }
        dispTeacher();
        selectTeacher();
    }

    /* 根据Name查找学生信息, 并打印, 待用户确认后删除该记录        */
    public void removeStudentByName() {
        System.out.print("Please input name: ");
        String str = getString();
        if (ls.searchByName(str)) {
            System.out.print("Record was found, REMOVE it please input y,\n"
                            +"or just press enter to cancel: ");
            if ((getString()).equalsIgnoreCase("y")) {
                ls.removeByName(str);
                System.out.print("Record was removed, press enter to continue... ");
                getString();
            }
        } else {
            System.out.print("Record not found, press enter to continue... ");
            getString();
        }
        dispStudent();
        selectStudent();
    }

    /* 添加示例师生信息
     * 当数据文件为空时可调用该方法生成示例数据        */
    public void addSample() {
        Teacher t = new Teacher("101", "Zhao", "Male", "English", "chief", "Accounting");
        lt.addLast(t);
        t = new Teacher("102", "Qian", "Male", "Chemisty", "chairmem", "Material");
        lt.addLast(t);
        t = new Teacher("103", "Sun", "Female", "Physics", "president", "Banking");
        lt.addLast(t);
        t = new Teacher("104", "Li", "Female", "Math", "secretary", "Mathematics");
        lt.addLast(t);

        Student s = new Student("201", "Zhou", "Female", "Accounting");
        ls.addLast(s);
        s = new Student("202", "Wu", "Male", "Electronics");
        ls.addLast(s);
        s = new Student("203", "Zheng", "Male", "Mathematics");
        ls.addLast(s);
        s = new Student("204", "Wang", "Female", "Banking");
        ls.addLast(s);
    }
}