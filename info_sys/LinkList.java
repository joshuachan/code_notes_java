/* ******************************************************
 * FileName: LinkList.java
 * Project: 师生信息管理系统
 * Description: 通过双向环形链表实现教师和学生信息的分开管理
 *              主要操作有:
 *              1. 记录添加
 *              2. 按ID或Name查找或删除记录
 *              3. 数据的自动导入和导出
 * Author & Date: Joshua Chan, 2011/11/14
 * *****************************************************/
/* 双向环形链表类定义        */
public class LinkList {
    private class Node {
        public Object data;
        public Node prev;
        public Node next;
    }

    private Node head;
    private int count;

    /* 链表初始化        */
    public LinkList() {
        head = new Node();

        head.prev = head;
        head.next = head;
        count = 0;
    }

    public void release() {
        head = null;
        count = 0;
    }

    public boolean isEmpty() {
        return count == 0;
    }

    /* 在尾部加入结点        */
    public void addLast(Object data) {
        Node n = new Node();

        n.data = data;
        n.prev = head.prev;
        n.next = head;
        head.prev.next = n;
        head.prev = n;

        count++;
    }

    /* 在头部加入结点        */
    public void addFirst(Object data) {
        Node n = new Node();

        n.data = data;
        n.prev = head;
        n.next = head.next;
        head.next.prev = n;
        head.next = n;

        count++;
    }

    /* 在指定序号处加入结点        */
    public void addAt(Object data, int index) {
        Node cur = head.next;
        Node n;

        if (index < 0)
            index = 0;
        else if (index >= count)
            index = count - 1;

        for (int i = 0; i < index; i++)
            cur = cur.next;
        n = new Node();
        n.data = data;
        n.prev = cur.prev;
        n.next = cur;
        cur.prev.next = n;
        cur.prev = n;
        count++;
    }

    /* 删除指定序号处结点        */
    public boolean removeAt(int index) {
        if (isEmpty() || index < 0 || index >= count)
            return false;

        Node cur = head.next;

        for (int i = 0; i < index; i++)
            cur = cur.next;

        cur.prev.next = cur.next;
        cur.next.prev = cur.prev;
        cur = null;
        count--;

        return true;
    }

    /* 根据ID删除结点        */
    public boolean removeByID(Object o) {
        if (isEmpty())
            return false;

        Person p;
        Node save;
        for (Node cur = head.next; cur != head; cur = save) {
            save = cur.next;
            p = (Person)cur.data;
            if (p.compID((String)o) == 0) {
                cur.prev.next = cur.next;
                cur.next.prev = cur.prev;
                cur = null;
                count--;
                return true;
            }
        }
        return false;
    }

    /* 根据Name删除结点        */
    public boolean removeByName(Object o) {
        if (isEmpty())
            return false;

        Person p;
        Node save;
        for (Node cur = head.next; cur != head; cur = save) {
            save = cur.next;
            p = (Person)cur.data;
            if (p.compName((String)o) == 0) {
                cur.prev.next = cur.next;
                cur.next.prev = cur.prev;
                cur = null;
                count--;
                return true;
            }
        }
        return false;
    }

    /* 根据ID查找并打印        */
    public boolean searchByID(Object o) {
        if (isEmpty())
            return false;

        Person p;
        for (Node cur = head.next; cur != head; cur = cur.next) {
            p = (Person)cur.data;
            if (p.compID((String)o) == 0) {
                if (p instanceof Teacher) {
                    Teacher t = (Teacher)p;
                    System.out.printf("\n%-6s%-10s%-10s%-14s%-14s%-14s\n", "ID", "Name",
                            "Gender", "Course", "Position", "Department");
                    System.out.println("--------------------------------------"
                                    + "------------------------------");
                    t.show();
                    System.out.println();
                } else if (p instanceof Student) {
                    Student s = (Student)p;
                    System.out.printf("\n%-6s%-10s%-10s%-14s\n", "ID", "Name",
                            "Gender", "Department");
                    System.out.println("----------------------------------------");
                    s.show();
                    System.out.println();
                }
                return true;
            }
        }
        return false;
    }

    /* 根据Name查找并打印        */
    public boolean searchByName(Object o) {
        if (isEmpty())
            return false;

        Person p;
        for (Node cur = head.next; cur != head; cur = cur.next) {
            p = (Person)cur.data;
            if (p.compName((String)o) == 0) {
                if (p instanceof Teacher) {
                    Teacher t = (Teacher)p;
                    System.out.printf("\n%-6s%-10s%-10s%-14s%-14s%-14s\n", "ID", "Name",
                            "Gender", "Course", "Position", "Department");
                    System.out.println("--------------------------------------"
                                    + "------------------------------");
                    t.show();
                    System.out.println();
                } else if (p instanceof Student) {
                    Student s = (Student)p;
                    System.out.printf("\n%-6s%-10s%-10s%-14s\n", "ID", "Name",
                            "Gender", "Department");
                    System.out.println("----------------------------------------");
                    s.show();
                    System.out.println();
                }
                return true;
            }
        }
        return false;
    }

    /* 遍历链表并打印        */
    public boolean travelList() {
        if (isEmpty())
            return false;

        Node cur = head.next;
        if (cur.data instanceof Teacher) {
            Teacher t;
            System.out.printf("\n%-6s%-10s%-10s%-14s%-14s%-14s\n", "ID", "Name",
                    "Gender", "Course", "Position", "Department");
            System.out.println("--------------------------------------"
                            + "------------------------------");
            for (; cur != head; cur = cur.next) {
                t = (Teacher)cur.data;
                t.show();
            }
            System.out.printf("Total: %d %s\n\n", count
                            ,(count>1 ? "records." : "record."));
        } else if (cur.data instanceof Student) {
            Student s;
            System.out.printf("\n%-6s%-10s%-10s%-14s\n", "ID", "Name",
                    "Gender", "Department");
            System.out.println("----------------------------------------");
            for (; cur != head; cur = cur.next) {
                s = (Student)cur.data;
                s.show();
            }
            System.out.printf("Total: %d %s\n\n", count
                            ,(count>1 ? "records." : "record."));
        }
        return true;
    }

    /* 从指定文件中导入数据
     * 记录加入到链表前需将String进行分割        */
    public boolean importList(String path) {
        String[] sa = null;
        String str = null;
        TextFileReader tfr = new TextFileReader();

        tfr.open(path);
        if ((str = tfr.readLine()) == null)
            return false;
        sa = str.split(",");

        if (sa.length == 6) {
            Teacher t;
            do {
                sa = str.split(",");
                t = new Teacher(sa[0], sa[1], sa[2], sa[3], sa[4], sa[5]);
                addLast(t);
            } while ((str = tfr.readLine()) != null);
        } else if (sa.length == 4) {
            Student s;
            do {
                sa = str.split(",");
                s = new Student(sa[0], sa[1], sa[2], sa[3]);
                addLast(s);
            } while ((str = tfr.readLine()) != null);
        }

        tfr.close();
        return true;
    }

    /* 导出数据到指定文件
     * 写入文件前将同一条记录的各字段以英文逗号进行连接        */
    public boolean exportList(String path) {
        if (isEmpty())
            return false;

        String str;
        Node cur = head.next;
        TextFileWriter tfw = new TextFileWriter();

        tfw.open(path);
        if (cur.data instanceof Teacher) { 
            Teacher t;
            for (; cur != head; cur = cur.next) {
                t = (Teacher)cur.data;
                str = t.getID() + "," + t.getName() + "," + t.getGender() + ","
                    + t.getCourse() + "," + t.getPosition() + "," + t.getDepart();
                tfw.writeLine(str);
            }
        } else if (cur.data instanceof Student) {
            Student s;
            for (; cur != head; cur = cur.next) {
                s = (Student)cur.data;
                str = s.getID() + "," + s.getName() + ","
                    + s.getGender() + "," + s.getDepart();
                tfw.writeLine(str);
            }
        }

        tfw.close();
        return true;
    }

    public int length() {
        return count;
    }

    public boolean removeFirst() {
        return removeAt(0);
    }

    public boolean removeLast() {
        return removeAt(count - 1);
    }
}