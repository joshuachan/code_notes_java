/* ******************************************************
 * FileName: Test.java
 * Project: 师生信息管理系统
 * Description: 通过双向环形链表实现教师和学生信息的分开管理
 *              主要操作有:
 *              1. 记录添加
 *              2. 按ID或Name查找或删除记录
 *              3. 数据的自动导入和导出
 * Author & Date: Joshua Chan, 2011/11/14
 * *****************************************************/

 /* 测试程序        */
public class Test {
    public static void main(String[] args) {
        /* 创建教师和学生链表类        */
        LinkList lt = new LinkList();
        LinkList ls = new LinkList();
        UserInterface in = new UserInterface(lt, ls);

        /* 分别导入运行目录下的教师和学生数据文件        */
        lt.importList("./Teacher.dat");
        ls.importList("./Student.dat");

        /* 生成示例数据
         * 仅当数据文件为空时调用
         * 在正常运行时应当注释掉        */
        in.addSample();

        /* 调用界面及方法        */
        in.dispMain();
        in.selectMain();

        /* 分别导出教师和学生信息至运行目录下的数据文件        */
        lt.exportList("./Teacher.dat");
        ls.exportList("./Student.dat");
    }
}