public class Table {
    private int n;
    private int[][] arr;
    
    Table(int m) {
        n = m;
        arr = new int[n][];
        for (int i = 0; i < n; i++)
            arr[i] = new int[i+1];

        for (int i = 0; i < arr.length; i++)
            for (int j = 0; j < arr[i].length; j++)
                arr[i][j] = (i + 1) * (j + 1);
    }

    public void printTable() {
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                System.out.printf("%4d", arr[i][j]);
            }
            System.out.println();
        }
    }
}