class Locker {
    private int count = 0;
    private int index = 0;

    public Locker(int count) {
        this.count = count;
        index = count;
    }

    public void enter() {
        while (true) {
            synchronized (this) {
                if (index > 0) {
                    index--;
                    break;
                } else {
                    try {
                        wait();
                    } catch (InterruptedException e) {
                    }
                }
            }
        }
    }

    public void exit() {
        synchronized (this) {
            if (index < count)
                index++;
            notifyAll();
        }
    }
}

class MyThread1 extends Thread {
    private int n = 0;
    private int sum = 0;
    private Locker locker = null;

    public MyThread1(int n) {
        this.n = n;
        locker = new Locker(1);
    }

    public void run() {
        // System.out.println("Thread 1 begin run...");
        locker.enter();
        for (int i = 1; i <= n; i++) {
            sum += i;
        }
        locker.exit();
    }

    public int getSum() {
        // System.out.println("Thread 1 begin get...");
        locker.enter();
        locker.exit();
        return sum;
    }
}

class MyThread2 extends Thread {
    private int n = 0;
    private int sum = 0;
    private Locker locker = null;

    public MyThread2(int n) {
        this.n = n;
        locker = new Locker(1);
    }

    public void run() {
        // System.out.println("Thread 2 begin run...");
        locker.enter();
        for (int i = 1; i <= n; i++) {
            sum += ((i % 2 == 0) ? (-1 * i) : i);
            // System.out.printf("sum[%d] = %d\n", i, sum);
        }
        locker.exit();
    }

    public int getSum() {
        // System.out.println("Thread 2 begin get...");
        locker.enter();
        locker.exit();
        return sum;
    }
}

public class Test1 {
    public static void main(String[] args) {
        MyThread1 mt1 = new MyThread1(9);
        MyThread2 mt2 = new MyThread2(9);

        mt1.start();
        mt2.start();

        try {
            Thread.sleep(1);
        } catch (InterruptedException e) {
        }

        System.out.println("Sum1 = " + mt1.getSum());
        System.out.println("Sum2 = " + mt2.getSum());
    }
}