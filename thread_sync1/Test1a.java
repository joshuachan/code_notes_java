class MyThread1 extends Thread {
    private int n = 0;
    private int sum = 0;

    public MyThread1(int n) {
        this.n = n;
    }

    public void run() {
        // System.out.println("Thread 1 begin run...");
        synchronized (this) {
            for (int i = 1; i <= n; i++) {
                sum += i;
            }
        }
    }

    public int getSum() {
        // System.out.println("Thread 1 begin get...");
        synchronized (this) {
            return sum;
        }
    }
}

class MyThread2 extends Thread {
    private int n = 0;
    private int sum = 0;

    public MyThread2(int n) {
        this.n = n;
    }

    public void run() {
        // System.out.println("Thread 2 begin run...");
        synchronized (this) {
            for (int i = 1; i <= n; i++) {
                sum += ((i % 2 == 0) ? (-1 * i) : i);
                // System.out.printf("sum[%d] = %d\n", i, sum);
            }
        }
    }

    public int getSum() {
        // System.out.println("Thread 2 begin get...");
        synchronized (this) {
            return sum;
        }
    }
}

public class Test1a {
    public static void main(String[] args) {
        MyThread1 mt1 = new MyThread1(9);
        MyThread2 mt2 = new MyThread2(9);

        mt1.start();
        mt2.start();

        try {
            Thread.sleep(1);
        } catch (InterruptedException e) {
        }

        System.out.println("Sum1 = " + mt1.getSum());
        System.out.println("Sum2 = " + mt2.getSum());
    }
}